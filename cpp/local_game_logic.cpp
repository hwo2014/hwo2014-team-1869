#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <jsoncons/json.hpp>

#include "car.h"
#include "local_game_logic.h"
#include "track.h"
#include "track_piece.h"

using namespace std;

LocalGameLogic::LocalGameLogic(const string& localConfig)
    : GameLogic()
    , mCarMetadata()
{
    static const string LOCAL = "local/";
    static const string TRACKS = "tracks/";
    static const string CARS = "cars/";
    static const string EXT = ".json";
    auto config = jsoncons::json::parse_file(LOCAL + localConfig + EXT);
    
    // track
    auto trackFile = LOCAL + TRACKS + config["trackName"].as<string>() + EXT;
    mTrack = new Track(jsoncons::json::parse_file(trackFile));
    mTrack->setTotalLaps(config["laps"].as<int>());

    // physics parameters
    auto& params = mTrack->getPhysicsParams();
    params.friction.setExplicitValue(config["friction"].as<double>());
    params.power.setExplicitValue(config["power"].as<double>());

    // bots
    for (size_t i = 0; i < config["bots"].size(); i++)
    {
        const auto bot = config["bots"][i];
        auto carName = bot["name"].as<string>();
        auto carFile = LOCAL + CARS + bot["color"].as<string>() + EXT;
        auto car = new Car(jsoncons::json::parse_file(carFile), mTrack);
        car->setName(carName);
        mCars.push_back(car);
        CarMetadata meta;
        if (bot.has_member("ghost"))
        {
            const auto filename = bot["ghost"].as<string>();
            meta.ghost.name = carName;
            loadGhost(filename, meta.ghost);
        }
        else
        {
            meta.vars = new PhysicsVariables();
        }
        mCarMetadata.push_back(meta);
    }
}

void LocalGameLogic::cleanUp()
{
    mCarMetadata.clear();
    GameLogic::cleanUp();
}

void LocalGameLogic::tick(int currentTick)
{
    if (currentTick == mCurrentTick)
    {
        return;
    }
    GameLogic::tick(currentTick);

    assert(mCars.size() == mCarMetadata.size());
    for (size_t i = 0; i < mCars.size(); i++)
    {
        auto car = mCars[i];
        auto location = car->getLocation();
        bool isGhost = !mCarMetadata[i].vars;
        if (isGhost)
        {
            doGhost(mCarMetadata[i].ghost, location);
        }
        else
        {
            doPhysics(mCarMetadata[i].vars, location);
        }
        car->updateLocation(location);
    }
}

void LocalGameLogic::doGhost(const GhostData& data, CarLocation &location)
{
    if (mCurrentTick < int(data.locs.size()))
    {
        location = data.locs[mCurrentTick];
    }
}

void LocalGameLogic::doPhysics(PhysicsVariables *vars, CarLocation &location)
{
    vars->throttle = 0.5;
    double accel;
    {
        const auto& params = mTrack->getPhysicsParams();
        const auto P = params.power.value();
        const auto T = vars->throttle;
        const auto v = vars->speed;
        const auto mu = params.friction.value();
        accel = P*(T - mu*v);
    }

    vars->speed += accel;
    location.inPieceDistance += vars->speed;
    const auto piece = mTrack->getPiece(location.pieceIndex);
    const auto laneLength = piece->getLaneLength(location.startLaneIndex,
                                                 location.endLaneIndex);
    if (location.inPieceDistance >= laneLength)
    {
        location.pieceIndex++;
        if (location.pieceIndex == mTrack->getPieceCount())
        {
            location.pieceIndex = 0;
            location.lap++;
        }
        location.inPieceDistance -= laneLength;
        location.startLaneIndex = location.endLaneIndex;
    }

    // TODO handle lane changes
    // TODO handle slip angle
}

void LocalGameLogic::loadGhost(const string& filename, GhostData& ghost)
{
    const auto root = jsoncons::json::parse_file(filename);
    for (size_t i = 0; i < root.size(); i++)
    {
        const auto& msg = root[i];
        const auto& msgType = msg["msgType"].as<string>();
        const auto& data = msg["data"];
        if (msgType == "gameInit")
        {
            // check we're in the good map
            assert(data["race"]["track"]["id"] == mTrack->getId());

            bool carFound = false;
            for (size_t j = 0; j < data["race"]["cars"].size(); j++)
            {
                const auto& car = data["race"]["cars"][j];
                if (car["id"]["name"] == ghost.name)
                {
                    carFound = true;
                    break;
                }
            }
            assert(carFound);
        }
        else if (msgType == "fullCarPositions")
        {
            for (size_t j = 0; j < data.size(); j++)
            {
                const auto& car = data[j];
                if (car["id"]["name"] == ghost.name)
                {
                    const auto& piece= car["piecePosition"];
                    const auto& lane= piece["lane"];
                    CarLocation loc;
                    loc.startLaneIndex = lane["startLaneIndex"].as<int>();
                    loc.endLaneIndex = lane["endLaneIndex"].as<int>();
                    loc.lap = piece["lap"].as<int>();
                    loc.pieceIndex = piece["pieceIndex"].as<int>();
                    loc.inPieceDistance = piece["inPieceDistance"].as<double>();
                    loc.slipAngle = car["angleOffset"].as<double>();
                    int gameTick;
                    if (msg.has_member("gameTick"))
                    {
                        gameTick = msg["gameTick"].as<int>();
                    }
                    else if (ghost.locs.empty())
                    {
                        gameTick = 0;
                    }
                    else
                    {
                        break;
                    }
                    assert(int(ghost.locs.size()) == gameTick);
                    ghost.locs.push_back(loc);
                    break;
                }
            }
        }
    }
}

