#include <cassert>
#include "protocol.h"

namespace hwo_protocol
{
    jsoncons::json makeRequest(const std::string& msg_type, const jsoncons::json& data)
    {
        jsoncons::json r;
        r["msgType"] = msg_type;
        r["data"] = data;
        return r;
    }

    jsoncons::json makeJoin(const std::string& name, const std::string& key)
    {
        jsoncons::json data;
        data["name"] = name;
        data["key"] = key;
        return makeRequest("join", data);
    }

    jsoncons::json makeJoinRace(const std::string& name,
                                const std::string& key,
                                const std::string& trackName,
                                int carCount)
    {
        jsoncons::json botId;
        botId["name"] = name;
        botId["key"] = key;

        jsoncons::json data;
        data["botId"] = botId;
        data["trackName"] = trackName;
        data["carCount"] = carCount;
        return makeRequest("joinRace", data);
    }

    jsoncons::json makePing()
    {
        return makeRequest("ping", jsoncons::null_type());
    }

    jsoncons::json makeThrottle(double throttle)
    {
        assert(throttle >= 0);
        assert(throttle <= 1);
        return makeRequest("throttle", throttle);
    }

    jsoncons::json makeSwitchLane(const std::string& direction)
    {
        assert(direction == "Left" || direction == "Right");
        return makeRequest("switchLane", direction);
    }

    jsoncons::json makeTurbo()
    {
        return makeRequest("turbo", "whooooaaaaa!!");
    }

}    // namespace hwo_protocol

