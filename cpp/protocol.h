#ifndef HWO_PROTOCOL_H
#define HWO_PROTOCOL_H

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>

namespace hwo_protocol
{
    jsoncons::json makeRequest(const std::string& msg_type, const jsoncons::json& data);
    jsoncons::json makeJoin(const std::string& name, const std::string& key);
    jsoncons::json makeJoinRace(const std::string& name,
                                const std::string& key,
                                const std::string& trackName,
                                int carCount);
    jsoncons::json makePing();
    jsoncons::json makeThrottle(double throttle);
    jsoncons::json makeSwitchLane(const std::string& direction);
    jsoncons::json makeTurbo();
}

#endif

