#include <cmath>
#include <string>

#include "util.h"

namespace util
{
    double deg2rad(double x)
    {
        return x * M_PI / 180.0;
    }

    double rad2deg(double x)
    {
        return x * 180.0 / M_PI;
    }

    double clamp(double x, double a, double b)
    {
        return std::min(std::max(x, a), b);
    }

    double easeInOutQuad(double t)
    {
        t = clamp(t, 0.0, 1.0);
        t *= 2.0;
        if (t < 1.0)
        {
            return 0.5*t*t;
        }
        return -0.5 * ((t-2.0)*(t-2.0) - 2.0);
    }

    double sqr(double x)
    {
        return x*x;
    }

    bool isZero(double x)
    {
        static const double EPSILON = 1e-6;
        return std::abs(x) < EPSILON;
    }
}

