#ifndef NETWORK_GAME_LOGIC_H
#define NETWORK_GAME_LOGIC_H

#include <string>
#include <map>
#include <functional>
#include <jsoncons/json.hpp>

#include "game_logic.h"

typedef std::vector<jsoncons::json> MsgVector;

class NetworkGameLogic : public GameLogic
{
public:
    NetworkGameLogic();
    virtual bool isLocal() const { return false; }

    MsgVector react(const jsoncons::json& msg);
    Car* getMyCar() const;

private:
    virtual void cleanUp();

    typedef std::function<void(NetworkGameLogic*, const jsoncons::json&)>
            ActionFun;
    const std::map<std::string, ActionFun> mActionMap;

    void parseJoin(const jsoncons::json& data);
    void parseYourCar(const jsoncons::json& data);
    void parseGameInit(const jsoncons::json& data);
    void parseGameStart(const jsoncons::json& data);
    void parseCarPositions(const jsoncons::json& data);
    void parseCrash(const jsoncons::json& data);
    void parseTurboAvailable(const jsoncons::json& data);
    void parseGameEnd(const jsoncons::json& data);
    void parseTournamentEnd(const jsoncons::json& data);
    void parseError(const jsoncons::json& data);

    std::string mMyCarName;
};

#endif

