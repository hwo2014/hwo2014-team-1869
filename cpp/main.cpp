#include <iostream>
#include <string>
#include "protocol.h"
#include "connection.h"
#include "local_game_logic.h"
#include "network_game_logic.h"
#if USE_OPENGL
#include <pthread.h>
#include "render.h"
#endif

using namespace hwo_protocol;

void runNetwork(HWO_Connection& connection, const std::string& name,
                const std::string& key)
{
    NetworkGameLogic game;
#if USE_OPENGL
    pthread_t th;
    pthread_create(&th, nullptr, runOpenGL, &game);
#endif
    connection.sendRequests({ makeJoin(name, key) });
    //connection.sendRequests({ makeJoinRace(name, key, "keimola", 1) });
    //connection.sendRequests({ makeJoinRace(name, key, "germany", 1) });
    //connection.sendRequests({ makeJoinRace(name, key, "usa", 1) });
    //connection.sendRequests({ makeJoinRace(name, key, "france", 1) });

    for (;;)
    {
        boost::system::error_code error;
        auto response = connection.receiveResponse(error);

        if (error == boost::asio::error::eof)
        {
            std::cout << "Connection close" << std::endl;
            break;
        }
        else if (error)
        {
            throw boost::system::system_error(error);
        }

        connection.sendRequests(game.react(response));
    }
}

void runLocal()
{
#if USE_OPENGL
    LocalGameLogic game("default");
    runOpenGL(&game);
#else
    throw std::runtime_error("runLocal executed without OpenGL support");
#endif
}

int main(int argc, const char* argv[])
{
    try
    {
        if (argc != 5)
        {
#if USE_OPENGL
            runLocal();
            return 0;
#else
            std::cerr << "Usage: ./run host port botname botkey" << std::endl;
            return 1;
#endif
        }

        const std::string host(argv[1]);
        const std::string port(argv[2]);
        const std::string name(argv[3]);
        const std::string key(argv[4]);
        std::cout << "Host: " << host << std::endl
                  << "Port: " << port << std::endl
                  << "Name: " << name << std::endl
                  << "Key : " << key  << std::endl;

        HWO_Connection connection(host, port);
        runNetwork(connection, name, key);
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return 2;
    }

    return 0;
}

