#ifndef LOCAL_GAME_LOGIC_H
#define LOCAL_GAME_LOGIC_H

#include <string>
#include <vector>

#include "car.h"
#include "game_logic.h"

struct GhostData
{
    std::string name;
    std::vector<CarLocation> locs;
};

struct PhysicsVariables
{
    PhysicsVariables() : throttle(), speed() {}
    double throttle;
    double speed;
};

struct CarMetadata
{
    CarMetadata() : vars(nullptr), ghost() {}
    PhysicsVariables *vars;
    GhostData ghost;
};

class LocalGameLogic : public GameLogic
{
public:
    LocalGameLogic(const std::string& configName);
    virtual bool isLocal() const { return true; }

    virtual void tick(int currentTick);

private:
    virtual void cleanUp();

    void doPhysics(PhysicsVariables *vars, CarLocation &location);
    void doGhost(const GhostData& data, CarLocation &location);
    void loadGhost(const std::string& filename, GhostData& ghost);

    std::vector<CarMetadata> mCarMetadata;
};

#endif

