#include <cassert>

#include "car.h"
#include "game_logic.h"
#include "track.h"

using namespace std;

GameLogic::GameLogic()
    : mTrack(nullptr)
    , mCars()
    , mCurrentTick(0)
{
}

GameLogic::~GameLogic()
{
    cleanUp();
}

void GameLogic::tick(int currentTick)
{
    mCurrentTick = currentTick;
}

void GameLogic::cleanUp()
{
    for (auto car : mCars)
    {
        delete car;
    }
    mCars.clear();

    delete mTrack;
    mTrack = nullptr;
}

Car* GameLogic::getCar(const std::string& carName) const
{
    for (auto car : mCars)
    {
        if (car->getID().name == carName)
        {
            return car;
        }
    }
    return nullptr;
}

