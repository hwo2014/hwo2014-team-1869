#include <cassert>

#include "car.h"
#include "lane.h"
#include "network_game_logic.h"
#include "protocol.h"
#include "track.h"
#include "track_piece.h"

using namespace hwo_protocol;

NetworkGameLogic::NetworkGameLogic()
    : GameLogic()
    , mActionMap
      {
          { "join", &NetworkGameLogic::parseJoin },
          { "yourCar", &NetworkGameLogic::parseYourCar },
          { "gameInit", &NetworkGameLogic::parseGameInit },
          { "gameStart", &NetworkGameLogic::parseGameStart },
          { "carPositions", &NetworkGameLogic::parseCarPositions },
          { "crash", &NetworkGameLogic::parseCrash },
          { "turboAvailable", &NetworkGameLogic::parseTurboAvailable },
          { "gameEnd", &NetworkGameLogic::parseGameEnd },
          { "tournamentEnd", &NetworkGameLogic::parseTournamentEnd },
          { "error", &NetworkGameLogic::parseError }
      }
    , mMyCarName("")
{
}

void NetworkGameLogic::cleanUp()
{
    GameLogic::cleanUp();
}

MsgVector NetworkGameLogic::react(const jsoncons::json& msg)
{
    const auto& msg_type = msg["msgType"].as<std::string>();
    const auto& data = msg["data"];
    auto action_it = mActionMap.find(msg_type);
    if (action_it != mActionMap.end())
    {
        (action_it->second)(this, data);
    }
    else
    {
        std::cout << "Unknown message type: " << msg_type << std::endl;
    }
    if (msg.has_member("gameTick"))
    {
        tick(msg["gameTick"].as<int>());
    }
    double T = 0.2;
    if (getMyCar())
    {
        auto loc = getMyCar()->getLocation();
        double alpha = (loc.lap*40 + loc.pieceIndex) / 119.0;
        double maxSpeed = 6.0 + alpha * 1.0;
        T = maxSpeed * mTrack->getPhysicsParams().friction.value();
        if (T <= 0)
        {
            T = 0.1;
        }
        else if (T > 1.0)
        {
            T = 1.0;
        }
        getMyCar()->getStats().throttle = T;
    }
    if (msg_type == "gameStart" ||
       (msg_type == "carPositions" && msg.has_member("gameTick")))
    {
        return { hwo_protocol::makeThrottle(T) };
    }
    return {};
}

void NetworkGameLogic::parseJoin(const jsoncons::json& data)
{
    std::cout << "Joined" << std::endl;
}

void NetworkGameLogic::parseYourCar(const jsoncons::json& data)
{
    mMyCarName = data["name"].as<std::string>();
    std::cout << "My car: " << mMyCarName
              << " (" << data["color"].as<std::string>() << ")"
              << std::endl;
}

void NetworkGameLogic::parseGameInit(const jsoncons::json& data)
{
    std::cout << "Game init" << std::endl;
    cleanUp();
    mTrack = new Track(data["race"]["track"]);
    if (data["race"]["raceSession"].has_member("laps"))
    {
        mTrack->setTotalLaps(data["race"]["raceSession"]["laps"].as<int>());
    }
    for (size_t i = 0; i < data["race"]["cars"].size(); i++)
    {
        auto car = new Car(data["race"]["cars"][i], mTrack);
        mCars.push_back(car);
        if (car->getID().name == mMyCarName)
        {
            car->setThisIsMyCar();
        }
    }
}

void NetworkGameLogic::parseGameStart(const jsoncons::json& data)
{
    std::cout << "Race started" << std::endl;
}

void NetworkGameLogic::parseCarPositions(const jsoncons::json& data)
{
    for (size_t i = 0; i < data.size(); i++)
    {
        const auto carName = data[i]["id"]["name"].as<std::string>();
        auto car = getCar(carName);
        if (car)
        {
            car->updateFromJSON(data[i]);
        }
    }
}

void NetworkGameLogic::parseCrash(const jsoncons::json& data)
{
    std::cout << "Someone crashed" << std::endl;
}

void NetworkGameLogic::parseTurboAvailable(const jsoncons::json& data)
{
    auto& params = mTrack->getPhysicsParams();
    params.turbo.available = true;
    params.turbo.durationTicks = data["turboDurationTicks"].as<int>();
    params.turbo.factor = data["turboFactor"].as<double>();
    std::cout << "Turbo available!" << std::endl;
    std::cout << "      duration: " << params.turbo.durationTicks << std::endl;
    std::cout << "      factor  : " << params.turbo.factor << std::endl;
}

void NetworkGameLogic::parseGameEnd(const jsoncons::json& data)
{
    std::cout << "Race ended" << std::endl;
}

void NetworkGameLogic::parseTournamentEnd(const jsoncons::json& data)
{
    std::cout << "Tournament ended" << std::endl;
    const auto& params = mTrack->getPhysicsParams();
    std::cout << "friction: " << params.friction << std::endl;
    std::cout << "power   : " << params.power << std::endl;
    std::cout << "damping : " << params.slipDamping << std::endl;
    std::cout << "restore : " << params.slipRestoring << std::endl;
    exit(0);
}

void NetworkGameLogic::parseError(const jsoncons::json& data)
{
    std::cout << "Error: " << data.to_string() << std::endl;
}

Car* NetworkGameLogic::getMyCar() const
{
    return getCar(mMyCarName);
}

