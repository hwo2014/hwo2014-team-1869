#include "vector2d.h"

Vector2D operator*(double c, const Vector2D &A)
{
    return Vector2D(c*A.x, c*A.y);
}

