#ifndef MATRIX2D_H
#define MATRIX2D_H

#include <cassert>
#include <iostream>

#include "vector2d.h"
#include "util.h"

struct Matrix2D
{
    double a, b,
           c, d;

    Matrix2D() : a(), b(), c(), d() {}

    Matrix2D operator*(double x) const 
    {
        Matrix2D result;
        result.a = a*x; result.b = b*x;
        result.c = c*x; result.d = d*x;
        return result;
    }

    Vector2D operator*(const Vector2D& v) const
    {
        Vector2D result;
        result.x = a*v.x + b*v.y;
        result.y = c*v.x + d*v.y;
        return result;
    }

    double det() const { return a*d - b*c; }
    bool isSingular() const { return util::isZero(det()); }

    Matrix2D inverse() const
    {
        assert(!isSingular());
        Matrix2D result;
        result.a = d;  result.b = -b;
        result.c = -c; result.d = a;
        return result * (1.0 / det());
    }

    friend std::ostream & operator<<(std::ostream &flux, const Matrix2D &m)
    {
        flux << m.a << " " << m.b << std::endl;
        flux << m.c << " " << m.d;
        return flux;
    }
};

#endif

