#include <cassert>
#include <fstream>
#include <iostream>
#include <string>

#include "car.h"
#include "lane.h"
#include "linear_solver.h"
#include "track.h"
#include "track_piece.h"
#include "util.h"

using namespace std;

Car::Car(const jsoncons::json& data, Track *track)
    : mTrack(track)
    , mIsMe(false)
    , mID()
    , mDim()
    , mLoc()
    , mStats()
    , mRecentHistory()
{
    mID.name = data["id"]["name"].as<string>();
    mID.color = data["id"]["color"].as<string>();

    mDim.length = data["dimensions"]["length"].as<double>();
    mDim.width = data["dimensions"]["width"].as<double>();
    mDim.flagPos = data["dimensions"]["guideFlagPosition"].as<double>();
}

TrackPiece* Car::getPiece() const
{
    return mTrack->getPiece(mLoc.pieceIndex);
}

Vector2D Car::getPosition() const
{
    return getPiece()->getPositionAlongLane
    (
        mLoc.inPieceDistance,
        mLoc.startLaneIndex,
        mLoc.endLaneIndex
    );
}

double Car::getRotation() const
{
    return -mLoc.slipAngle + getPiece()->getRotationAlongLane
    (
        mLoc.inPieceDistance,
        mLoc.startLaneIndex,
        mLoc.endLaneIndex
    );
}

void Car::updateFromJSON(const jsoncons::json& data)
{
    assert(mID.name == data["id"]["name"].as<string>());
    assert(mID.color == data["id"]["color"].as<string>());
    const auto& piece = data["piecePosition"];

    CarLocation newLocation;
    newLocation.slipAngle = data["angle"].as<double>();
    newLocation.lap = piece["lap"].as<int>();
    newLocation.pieceIndex = piece["pieceIndex"].as<int>();
    newLocation.inPieceDistance = piece["inPieceDistance"].as<double>();
    newLocation.startLaneIndex = piece["lane"]["startLaneIndex"].as<int>();
    newLocation.endLaneIndex = piece["lane"]["endLaneIndex"].as<int>();
    updateLocation(newLocation);
}

void Car::updateLocation(const CarLocation& newLocation)
{
    const auto& oldLocation = mLoc;
    double distanceTravelled = newLocation.inPieceDistance
                             - oldLocation.inPieceDistance;
    if (newLocation.pieceIndex != oldLocation.pieceIndex)
    {
        const auto oldPiece = mTrack->getPiece(oldLocation.pieceIndex);
        const auto len = oldPiece->getLaneLength(oldLocation.startLaneIndex,
                                                 oldLocation.endLaneIndex);
        distanceTravelled += len;
    }

    CarStats newStats;
    const auto& oldStats = mStats;
    newStats.speed = distanceTravelled;
    newStats.accel = newStats.speed - oldStats.speed;
    newStats.slipSpeed = newLocation.slipAngle - oldLocation.slipAngle;
    newStats.slipAccel = newStats.slipSpeed - oldStats.slipSpeed;
    newStats.throttle = oldStats.throttle;

    mLoc = newLocation;
    mStats = newStats;

    CarHistory history;
    history.loc = newLocation;
    history.stats = newStats;
    mRecentHistory.push_back(history);
    while (mRecentHistory.size() > 4)
    {
        mRecentHistory.erase(mRecentHistory.begin());
    }

    estimatePhysicsParams();
}

void Car::estimatePhysicsParams()
{
    if (isMe())
    {
        estimateLinearMotionParams();
        estimateSlipDynamicsParams();
    }
    else
    {
        estimateThrottle();
    }
}

void Car::estimateLinearMotionParams()
{
    const int N = mRecentHistory.size();
    if (N < 2)
    {
        return;
    }
    const auto prevS = mRecentHistory[N-2].stats;
    const auto currS = mRecentHistory[N-1].stats;
    /*
       a = P*(T - mu*v)

       where
       a: acceleration
       P: engine power
       T: throttle
       mu: friction coefficient
       v: speed
    */
    LinearSolver solver;
    solver.addPoint(prevS.accel, prevS.throttle, prevS.speed);
    solver.addPoint(currS.accel, currS.throttle, currS.speed);
    if (!solver.isSolvable())
    {
        return;
    }
    const auto soln = solver.solve();
    if (util::isZero(soln.x))
    {
        return;
    }
    PhysicsParameters& params = mTrack->getPhysicsParams();
    const auto P = soln.x;
    const auto mu = -soln.y/soln.x;
    params.power.addSample(P);
    params.friction.addSample(mu);
}

void Car::estimateSlipDynamicsParams()
{
    const int N = mRecentHistory.size();
    if (N < 2)
    {
        return;
    }

    // make sure we've been on a straight piece for long enough
    for (int i = 0; i < 2; i++)
    {
        auto piece = mTrack->getPiece(mRecentHistory[N-1-i].loc.pieceIndex);
        if (piece->isCurve())
        {
            return;
        }
    }

    const auto prevS = mRecentHistory[N-2].stats;
    const auto currS = mRecentHistory[N-1].stats;
    const auto prevL = mRecentHistory[N-2].loc;
    const auto currL = mRecentHistory[N-1].loc;
    /*
       T'' = -G*T' - w*T

       where
       T'' (theta'') slip acceleration
       T' (theta') slip speed
       T (theta) slip angle
       G (gamma) slip damping coefficient
       w (omega) slip restoring coefficient
    */
    LinearSolver solver;
    solver.addPoint(prevS.slipAccel, prevS.slipSpeed, prevL.slipAngle);
    solver.addPoint(currS.slipAccel, currS.slipSpeed, currL.slipAngle);
    if (!solver.isSolvable())
    {
        return;
    }
    const auto soln = solver.solve();
    const auto G = -soln.x;
    const auto w = -soln.y;

    auto& params = mTrack->getPhysicsParams();
    params.slipDamping.addSample(G);
    params.slipRestoring.addSample(w);
}

void Car::estimateThrottle()
{
    // evaluate other cars' throttle
    auto& params = mTrack->getPhysicsParams();
    const auto P = params.power.value();
    const auto mu = params.friction.value();
    if (util::isZero(P))
    {
        return;
    }
    const auto v = mStats.speed;
    const auto a = mStats.accel;
    const auto T = a/P + mu*v;
    mStats.throttle = util::clamp(T, 0.0, 1.0);
}

double Car::getRelativePieceDistance() const
{
    const auto len = getPiece()->getLaneLength(mLoc.startLaneIndex,
                                               mLoc.endLaneIndex);
    return util::clamp(mLoc.inPieceDistance / len, 0.0, 1.0);
}

bool Car::operator<(const Car& other) const
{
    if (mLoc.lap != other.mLoc.lap)
    {
        return mLoc.lap < other.mLoc.lap;
    }
    // we're in the same lap, so check pieces
    if (mLoc.pieceIndex != other.mLoc.pieceIndex)
    {
        return mLoc.pieceIndex < other.mLoc.pieceIndex;
    }
    // we're in the same piece, so check relative distance travelled
    return getRelativePieceDistance() < other.getRelativePieceDistance();
}

