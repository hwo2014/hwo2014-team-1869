#ifndef CAR_H
#define CAR_H

#include <iostream>
#include <string>
#include <jsoncons/json.hpp>

#include "vector2d.h"

class TrackPiece;
class Track;

struct CarID
{
    CarID() : name(), color() {}
    std::string name;
    std::string color;
};

struct CarDimensions
{
    CarDimensions() : length(), width(), height(15.0), flagPos() {}
    double length;
    double width;
    double height;
    double flagPos;
};

struct CarLocation
{
    CarLocation()
        : startLaneIndex()
        , endLaneIndex()
        , lap()
        , pieceIndex()
        , inPieceDistance()
        , slipAngle()
    {}
    int startLaneIndex;
    int endLaneIndex;
    int lap;
    int pieceIndex;
    double inPieceDistance;
    double slipAngle;
};

struct CarStats
{
    CarStats() : speed() , accel() , throttle(), slipSpeed(), slipAccel() {}
    double speed;
    double accel;
    double throttle;
    double slipSpeed;
    double slipAccel;
};

struct CarHistory
{
    CarHistory() : loc(), stats() {}
    CarLocation loc;
    CarStats stats;
};

class Car
{
public:
    Car(const jsoncons::json& data, Track *track);

    Vector2D getPosition() const;
    double getRotation() const;

    TrackPiece* getPiece() const;
    double getRelativePieceDistance() const;
    bool operator<(const Car& other) const;

    void setName(const string& name) { mID.name = name; }
    const CarID& getID() const { return mID; }
    const CarLocation& getLocation() const { return mLoc; }
    const CarStats& getStats() const { return mStats; }
    CarStats& getStats() { return mStats; }

    void updateFromJSON(const jsoncons::json& data);
    void updateLocation(const CarLocation& newLocation);

    void setThisIsMyCar() { mIsMe = true; }
    bool isMe() const { return mIsMe; }

#if USE_OPENGL
    void draw() const;
    void drawHUD() const;
#endif

private:
    void estimatePhysicsParams();
    void estimateLinearMotionParams();
    void estimateSlipDynamicsParams();
    void estimateThrottle();

    Track *mTrack;
    bool mIsMe;

    CarID mID;
    CarDimensions mDim;
    CarLocation mLoc;
    CarStats mStats;

    std::vector<CarHistory> mRecentHistory;
};

#endif

