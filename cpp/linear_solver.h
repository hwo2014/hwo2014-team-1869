#ifndef LINEAR_SOLVER_H
#define LINEAR_SOLVER_H

#include "matrix2d.h"
#include "vector2d.h"

class LinearSolver
{
public:
    LinearSolver();
    void addPoint(double L, double R1, double R2);
    bool isSolvable() const;
    Vector2D solve() const;

private:
    Vector2D mVec;
    Matrix2D mMat;
    int mNumPoints;
};

#endif

