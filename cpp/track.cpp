#include <iostream>

#include "lane.h"
#include "track.h"
#include "track_piece.h"

using namespace std;

Track::Track(const jsoncons::json& data)
    : mId("")
    , mName("")
    , mPieces()
    , mLanes()
{
    mId = data["id"].as<string>();
    mName = data["name"].as<string>();

    for (size_t i = 0; i < data["lanes"].size(); i++)
    {
        const auto& laneNode = data["lanes"][i];
        const int index = laneNode["index"].as<int>();
        const double dist = laneNode["distanceFromCenter"].as<double>();
        mLanes.push_back(new Lane(index, dist));
    }

    Vector2D pos;
    double rot = 0;
    for (size_t i = 0; i < data["pieces"].size(); i++)
    {
        const auto& pieceNode = data["pieces"][i];
        const bool sw = pieceNode.get("switch", false).as<bool>();
        const int index = mPieces.size();
        TrackPiece *newPiece = nullptr;
        if (pieceNode.has_member("length"))
        {
            const double length = pieceNode["length"].as<double>();
            newPiece = new TrackPieceStraight(length, sw, this, index);
        }
        else
        {
            const double radius = pieceNode["radius"].as<double>();
            const double angle = pieceNode["angle"].as<double>();
            newPiece = new TrackPieceBend(radius, angle, sw, this, index);
        }
        newPiece->setStartPosition(pos);
        newPiece->setStartRotation(rot);
        mPieces.push_back(newPiece);

        pos = newPiece->getEndPosition();
        rot = newPiece->getEndRotation();
    }
}

const Lane* Track::getLane(int laneIndex) const
{
    for (auto lane : mLanes)
    {
        if (lane->index == laneIndex)
        {
            return lane;
        }
    }
    return nullptr;
}

PhysicsParameters::PhysicsParameters()
    : friction(1.0 / 10)
    , power(1.0 / 5)
    , slipDamping(1.0 / 10)
    , slipRestoring(1.0 / 112)
    , turbo()
{
}

