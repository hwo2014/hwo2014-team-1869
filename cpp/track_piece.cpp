#include <cassert>
#include "lane.h"
#include "track.h"
#include "track_piece.h"

using namespace std;

TrackPiece::TrackPiece(bool sw, const Track *track, int index)
    : mTrack(track)
    , mPieceIndex(index)
    , mSwitch(sw)
    , mStartPosition()
    , mStartRotation(0.0)
    , mWidth(60.0)
{
}

TrackPiece* TrackPiece::getNextPiece() const
{
    int nextPieceIndex = mPieceIndex + 1;
    if (nextPieceIndex == mTrack->getPieceCount())
    {
        nextPieceIndex = 0;
    }
    return mTrack->getPiece(nextPieceIndex);
}

Vector2D TrackPiece::getPositionAlongLane
(
    double dist,
    int startLaneIndex,
    int endLaneIndex
) const
{
    if (startLaneIndex == endLaneIndex)
    {
        return getPositionAlongLane(dist, startLaneIndex);
    }
    const double avgLength = getLaneLength(startLaneIndex, endLaneIndex);
    const Vector2D startPos = getPositionAlongLane(dist, startLaneIndex);
    const Vector2D endPos = getPositionAlongLane(dist, endLaneIndex);
    const double alpha = util::easeInOutQuad(dist / avgLength);
    return (1-alpha)*startPos + alpha*endPos;
}

double TrackPiece::getRotationAlongLane
(
    double dist,
    int startLaneIndex,
    int endLaneIndex
) const
{
    if (startLaneIndex == endLaneIndex)
    {
        return getRotationAlongLane(dist, startLaneIndex);
    }
    const double avgLength = getLaneLength(startLaneIndex, endLaneIndex);
    const double startRot = getRotationAlongLane(dist, startLaneIndex);
    const double endRot = getRotationAlongLane(dist, endLaneIndex);
    const double alpha = util::easeInOutQuad(dist / avgLength);
    return alpha*startRot + (1-alpha)*endRot;
}

double TrackPiece::getLaneLength(int startLaneIndex, int endLaneIndex) const
{
    if (startLaneIndex == endLaneIndex)
    {
        return getLaneLength(startLaneIndex);
    }
    const auto startLength = getLaneLength(startLaneIndex);
    const auto endLength = getLaneLength(endLaneIndex);
    // TODO find a nice way to compute length in curves
#if 0
    const auto distBetweenLanes =
        abs(mTrack->getLane(startLaneIndex)->distanceFromCenter -
            mTrack->getLane(endLaneIndex)->distanceFromCenter);
    return sqrt(util::sqr(endLength) + util::sqr(distBetweenLanes));
#else
    return 0.5*(startLength+endLength) + 2;
#endif
}

double TrackPiece::getLaneDistanceToNextSwitch(int laneIndex) const
{
    double dist = getLaneLength(laneIndex);
    const TrackPiece *nextPiece = getNextPiece();
    if (nextPiece->canSwitch())
    {
        return dist;
    }
    return dist + nextPiece->getLaneDistanceToNextSwitch(laneIndex);
}

//-----------------------------------------------------------------------------
// Straight Piece
//-----------------------------------------------------------------------------

TrackPieceStraight::TrackPieceStraight(double length, bool sw,
                                       const Track *track, int index)
    : TrackPiece(sw, track, index)
    , mLength(length)
{
}

Vector2D TrackPieceStraight::getEndPosition() const
{
    return mStartPosition + Vector2D(mLength, 0).rotatedBy(mStartRotation);
}

double TrackPieceStraight::getEndRotation() const
{
    return mStartRotation;
}

double TrackPieceStraight::getLaneLength(int laneIndex) const
{
    return mLength;
}

Vector2D
TrackPieceStraight::getPositionAlongLane(double dist, int laneIndex) const
{
    Vector2D delta(dist, -mTrack->getLane(laneIndex)->distanceFromCenter);
    return mStartPosition + delta.rotatedBy(mStartRotation);
}

double
TrackPieceStraight::getRotationAlongLane(double dist, int laneIndex) const
{
    return mStartRotation;
}

bool TrackPieceStraight::isCurve() const
{
    return false;
}

double TrackPieceStraight::getRadius() const
{
    return numeric_limits<double>::infinity();
}

//-----------------------------------------------------------------------------
// Bend Piece
//-----------------------------------------------------------------------------

TrackPieceBend::TrackPieceBend(double radius, double angle, bool sw,
                               const Track *track, int index)
    : TrackPiece(sw, track, index)
    , mRadius(radius)
    , mArcAngle(angle)
{
}

bool TrackPieceBend::isRightTurn() const
{
    return mArcAngle >= 0;
}

Vector2D TrackPieceBend::getEndPosition() const
{
    Vector2D pivot(0, isRightTurn() ? mRadius : -mRadius);
    Vector2D delta = pivot.rotatedBy(-mArcAngle) - pivot;
    return mStartPosition + delta.rotatedBy(mStartRotation);
}

double TrackPieceBend::getEndRotation() const
{
    return mStartRotation - mArcAngle;
}

double TrackPieceBend::getLaneRadius(int laneIndex) const
{
    double laneDist = mTrack->getLane(laneIndex)->distanceFromCenter;
    return mRadius + (isRightTurn() ? -laneDist : laneDist);
}

double TrackPieceBend::getLaneLength(int laneIndex) const
{
    return getLaneRadius(laneIndex) * util::deg2rad(abs(mArcAngle));
}

Vector2D TrackPieceBend::getPositionAlongLane(double dist, int laneIndex) const
{
    Vector2D pivot(0, isRightTurn() ? 1 : -1);
    double arcAngle = util::rad2deg(dist / getLaneRadius(laneIndex));
    Vector2D dir = pivot.rotatedBy(isRightTurn() ? -arcAngle : arcAngle);
    Vector2D delta = getLaneRadius(laneIndex) * dir - mRadius * pivot;
    return mStartPosition + delta.rotatedBy(mStartRotation);
}

double TrackPieceBend::getRotationAlongLane(double dist, int laneIndex) const
{
    double arcAngle = util::rad2deg(dist / getLaneRadius(laneIndex));
    arcAngle *= isRightTurn() ? -1 : 1;
    return mStartRotation + arcAngle;
}

bool TrackPieceBend::isCurve() const
{
    return true;
}

double TrackPieceBend::getRadius() const
{
    return mRadius;
}


