#include "linear_solver.h"

LinearSolver::LinearSolver()
    : mVec()
    , mMat()
    , mNumPoints(0)
{
}

void LinearSolver::addPoint(double L, double R1, double R2)
{
    assert(mNumPoints < 2);
    if (mNumPoints == 0)
    {
        mVec.x = L;
        mMat.a = R1;
        mMat.b = R2;
    }
    else
    {
        mVec.y = L;
        mMat.c = R1;
        mMat.d = R2;
    }
    mNumPoints++;
}

bool LinearSolver::isSolvable() const
{
    if (mNumPoints < 2)
    {
        return false;
    }
    return !mMat.isSingular();
}

Vector2D LinearSolver::solve() const
{
    assert(isSolvable());
    return mMat.inverse() * mVec;
}

