#ifndef UTIL_H
#define UTIL_H

namespace util
{
    double deg2rad(double x);
    double rad2deg(double x);
    double clamp(double x, double a, double b);
    double easeInOutQuad(double alpha);
    double sqr(double x);
    bool isZero(double x);
}

#endif

