#ifndef LANE_H
#define LANE_H

#include <string>

struct Lane
{
    Lane(int i, double d) : index(i), distanceFromCenter(d) {}

    int index;
    double distanceFromCenter;

    std::string getColor() const
    {
        if (index == 0) return "red";
        if (index == 1) return "blue";
        if (index == 2) return "green";
        return "orange";
    }
};

#endif

