#ifndef TRUNCATED_ESTIMATOR_H
#define TRUNCATED_ESTIMATOR_H

#include <vector>

class TruncatedEstimator
{
public:
    TruncatedEstimator(double estimate);
    void addSample(double sample);
    double value() const { return mTruncatedMean; }
    double stdDev() const { return mTruncatedStdDev; }
    void setExplicitValue(double newVal) { mTruncatedMean = newVal; }
    int getSampleCount() const { return mData.size(); }
    void debugDump() const;

private:
    double mTruncatedMean;
    double mTruncatedStdDev;
    std::vector<double> mData;
};

std::ostream& operator<<(std::ostream &flux, const TruncatedEstimator &e);

#endif

