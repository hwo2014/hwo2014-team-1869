#include <algorithm>
#include <cassert>
#include <iostream>

#include "truncated_estimator.h"
#include "util.h"

using namespace std;

TruncatedEstimator::TruncatedEstimator(double estimate)
    : mTruncatedMean(estimate)
    , mTruncatedStdDev()
    , mData()
{
}

void TruncatedEstimator::addSample(double sample)
{
    // median is the most robust estimator
    auto it = lower_bound(mData.begin(), mData.end(), sample);
    mData.insert(it, sample);
    const int N = mData.size();
    const auto truncate = N / 8;
    if (truncate < 1)
    {
        return;
    }
    double sum = 0.0;
    for (int i = truncate; i < N-truncate; i++)
    {
        sum += mData[i];
    }
    mTruncatedMean = sum / (N - 2*truncate);

    double sumSqr = 0.0;
    for (int i = truncate; i < N-truncate; i++)
    {
        sumSqr += util::sqr(mData[i] - mTruncatedMean);
    }
    const auto variance = sumSqr / (N - 2*truncate - 1);
    mTruncatedStdDev = sqrt(variance);
}

std::ostream& operator<<(std::ostream &flux, const TruncatedEstimator &e)
{
    flux << e.value() << " ± " << e.stdDev();
    return flux;
}

void TruncatedEstimator::debugDump() const
{
    const int N = mData.size();
    const auto truncate = N / 4;

    double sumAll = 0.0;
    double sumRed = 0.0;
    int countAll = 0;
    int countRed = 0;
    cout << "-----begin data-----" << endl;
    for (int i = 0; i < N; i++)
    {
        if (i == truncate)
        {
            cout << "-----begin trunc-----" << endl;
        }
        if (i >= truncate && i < N-truncate)
        {
            sumRed += mData[i];
            countRed++;
        }
        sumAll += mData[i];
        countAll++;
        cout << 1.0 / mData[i] << endl;
        if (i == N-truncate)
        {
            cout << "-----end trunc-----" << endl;
        }
    }
    cout << "-----end data-----" << endl;
    cout << "mean all: " << (sumAll / countAll) << endl;
    cout << "mean red: " << (sumRed / countRed) << endl;
}

