#if USE_OPENGL

#include <cassert>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <GL/gl.h>
#include <GL/glut.h>

#include "car.h"
#include "game_logic.h"
#include "lane.h"
#include "render.h"
#include "track.h"
#include "track_piece.h"
#include "util.h"

using namespace std;

#define W 1920
#define H 1080

GameLogic *gGameLogic = nullptr;

class Color
{
public:
    Color(int r, int g, int b) : r(r), g(g), b(b) {}

    static Color fromName(const std::string& name)
    {
        if (name == "red") return Color(255, 0, 0);
        if (name == "green") return Color(0, 181, 24);
        if (name == "blue") return Color(0, 0, 255);
        if (name == "black") return Color(0, 0, 0);
        if (name == "white") return Color(255, 255, 255);
        if (name == "orange") return Color(255, 98, 0);
        if (name == "cyan") return Color(0, 255, 255);
        if (name == "magenta") return Color(255, 0, 255);
        if (name == "yellow") return Color(255, 255, 0);
        if (name == "purple") return Color(128, 0, 128);
        if (name == "lightred") return Color(255, 128, 128);
        if (name == "lightblue") return Color(128, 128, 255);
        if (name == "lightyellow") return Color(255, 255, 128);
        if (name == "lightgreen") return Color(128, 255, 128);
        if (name == "lightorange") return Color(255, 176, 0);

        return Color(255, 130, 186); // default, light pink
    }

    Color niceContrast() const
    {
        if (r+g+b < 255*3/2)
        {
            return fromName("white");
        }
        return fromName("black");
    }

    void apply() const
    {
        glColor3d(r/255.0, g/255.0, b/255.0);
    }

    Color operator*(double alpha) const
    {
        return Color(int(r*alpha), int(g*alpha), int(b*alpha));
    }

    Color operator+(const Color& o) const
    {
        return Color(r+o.r, g+o.g, b+o.b);
    }

private:
    int r;
    int g;
    int b;
};

class TextBox
{
public:
    TextBox(const Color& backColor)
        : mX(0)
        , mY(0)
        , mBackgroundColor(backColor)
        , mTextColor(backColor.niceContrast())
        , mStringStream()
        , mMinLineLength(0)
    {
    }

    template<typename T>
    TextBox& operator<<(const T& v)
    {
        mStringStream << v;
        return *this;
    }

    void draw() const
    {
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        glOrtho(0, W, H, 0, -1, 1);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();
        glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_LIGHTING_BIT);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);

        vector<string> lines;
        int maxLength;
        toLines(lines, maxLength);

        // background quad
        {
            mBackgroundColor.apply();
            int quadWidth, quadHeight;
            getBoxDimensions(lines, maxLength, quadWidth, quadHeight);
            glBegin(GL_QUADS);
            glVertex3i(mX, mY+quadHeight, 0);
            glVertex3i(mX+quadWidth, mY+quadHeight, 0);
            glVertex3i(mX+quadWidth, mY, 0);
            glVertex3i(mX, mY, 0);
            glEnd();
        }

        // lines of text
        mTextColor.apply();
        int x = mX + MARGIN;
        int y = mY + MARGIN + CHAR_H;
        for (const auto& line : lines)
        {
            glRasterPos2i(x, y);
            for (const auto c : line)
            {
                glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c);
            }
            y += CHAR_H + MARGIN;
        }

        glPopAttrib();
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
    }

    void setPosition(int x, int y)
    {
        mX = x;
        mY = y;
    }

    void setMinLineLength(int minLength)
    {
        mMinLineLength = minLength;
    }

    void getBoxDimensions(int &width, int &height) const
    {
        vector<string> lines;
        int maxLength;
        toLines(lines, maxLength);
        getBoxDimensions(lines, maxLength, width, height);
    }

    void getBoxDimensions(const vector<string>& lines, int maxLength,
                          int &width, int &height) const
    {
        width = 2*MARGIN + CHAR_W*maxLength;
        height = 2*MARGIN + (CHAR_H+MARGIN)*lines.size();
    }

private:
    void toLines(vector<string>& lines, int& maxLength) const
    {
        maxLength = 0;
        lines.clear();
        stringstream ss(mStringStream.str());
        for (;;)
        {
            string line;
            getline(ss, line);
            if (ss.eof())
            {
                break;
            }
            lines.push_back(line);
            maxLength = max(maxLength, int(line.size()));
        }
        maxLength = max(maxLength, mMinLineLength);
    }

    int mX;
    int mY;
    Color mBackgroundColor;
    Color mTextColor;
    stringstream mStringStream;
    int mMinLineLength;

    static const int CHAR_W = 9;
    static const int CHAR_H = 15;
    static const int MARGIN = 4;
};


struct MouseEvent
{
    MouseEvent()
        : button(-1)
        , x(-1)
        , y(-1) {}
    int button;
    int x;
    int y;
} gLastMouseEvt;

struct CamInfo
{
    CamInfo()
        : fovy(45.0)
        , ratio(W / (double) H)
        , my_near(0.2)
        , my_far(3000.0)
        , theta(80.0)
        , phi(10.0)
        , r(10.0) {}

    double fovy;
    double ratio;
    double my_near;
    double my_far;
    double theta;
    double phi;
    double r;
} gCam;

struct Light
{
    GLenum lightID; // GL_LIGHT0, GL_LIGHT1, etc.
    float ambient[4];
    float diffuse[4];
    float specular[4];
    float position[4];

    // attenuation
    float Kc; // constant
    float Kl; // linear
    float Kq; // quadractic

    bool on;
} gLight;

struct Material
{
    float ambient[4];
    float diffuse[4];
    float specular[4];
    float shininess;
} gMaterial;

bool gWireframe = false;
bool gCullFaces = false;
bool gLighting = true;

int gCarBoxY = 0;

void setCamera()
{
    float x = gCam.r
            * sin(util::deg2rad(gCam.theta))*cos(util::deg2rad(gCam.phi));
    float y = gCam.r
            * sin(util::deg2rad(gCam.theta))*sin(util::deg2rad(gCam.phi));
    float z = gCam.r * cos(util::deg2rad(gCam.theta));

    gluLookAt(x, y, z,
                0.0, 0.0, 0.0,
                0.0, 0.0, 1.0);
}

void setLighting(const Light& light)
{
    if (light.on)
    {
        glLightfv(light.lightID, GL_AMBIENT,    light.ambient);
        glLightfv(light.lightID, GL_DIFFUSE,    light.diffuse);
        glLightfv(light.lightID, GL_SPECULAR, light.specular);
        glLightfv(light.lightID, GL_POSITION, light.position);
        glLightf(light.lightID, GL_CONSTANT_ATTENUATION,    light.Kc);
        glLightf(light.lightID, GL_LINEAR_ATTENUATION,    light.Kl);
        glLightf(light.lightID, GL_QUADRATIC_ATTENUATION, light.Kq);

        glEnable(light.lightID);
    }
    else
    {
        glDisable(light.lightID);
    }
}

void setMaterial(const Material& mat)
{
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat.ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat.diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,mat.specular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, mat.shininess);
}

void display()
{
    glClearColor(21/255.0, 17/255.0, 27/255.0, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(gCam.fovy, gCam.ratio, gCam.my_near, gCam.my_far);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    setCamera();
    setLighting(gLight);
    setMaterial(gMaterial);

    gGameLogic->draw();

    glutSwapBuffers();
}

void idle()
{
    glutPostRedisplay();
}

void reshape(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(gCam.fovy, w/(double)h, gCam.my_near, gCam.my_far);
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'q':
        case 27: // escape
        {
            exit(0);
            break;
        }
        case 'w':
        {
            gWireframe = !gWireframe;
            if (gWireframe)
            {
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            }
            else
            {
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            }
            break;
        }
        case 'c':
        {
            gCullFaces = !gCullFaces;
            if (gCullFaces)
            {
                glEnable(GL_CULL_FACE);
            }
            else
            {
                glDisable(GL_CULL_FACE);
            }
            break;
        }
        case 'l':
        {
            gLighting = !gLighting;
            if (gLighting)
            {
                glEnable(GL_LIGHTING);
            }
            else
            {
                glDisable(GL_LIGHTING);
            }
            break;
        }
    }
}

void mouse(int button, int state, int x, int y)
{
    if (state == GLUT_DOWN)
    {
        gLastMouseEvt.button = button;
        gLastMouseEvt.x = x;
        gLastMouseEvt.y = y;
    }
    else if (state == GLUT_UP)
    {
        gLastMouseEvt.button = -1;
        gLastMouseEvt.x = -1;
        gLastMouseEvt.y = -1;
    }
}

void motion(int x, int y)
{
    int dx = x - gLastMouseEvt.x;
    int dy = y - gLastMouseEvt.y;

    gLastMouseEvt.x = x;
    gLastMouseEvt.y = y;

    switch(gLastMouseEvt.button)
    {
        case GLUT_LEFT_BUTTON:
        {
            // rotation
            gCam.phi     -= dx/2.0;
            gCam.theta -= dy/2.0;
            if (gCam.theta > 179)
                gCam.theta =    179;
            if (gCam.theta < 1)
                gCam.theta = 1;
            break;
        }
        case GLUT_RIGHT_BUTTON:
        {
            // zoom
            gCam.r += dy / 8.0;
            if(gCam.r < 1)
                gCam.r = 1;
        }
    }
    glutPostRedisplay();
}

void initLighting()
{
    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);

    glColorMaterial(GL_FRONT, GL_DIFFUSE);
    glColorMaterial(GL_FRONT, GL_AMBIENT);
    glEnable(GL_COLOR_MATERIAL);

    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);

    gLight.lightID = GL_LIGHT0;
    gLight.ambient[0] = 0.4;
    gLight.ambient[1] = 0.4;
    gLight.ambient[2] = 0.4;
    gLight.ambient[3] = 1.0;
    gLight.diffuse[0] = 1.0;
    gLight.diffuse[1] = 1.0;
    gLight.diffuse[2] = 1.0;
    gLight.diffuse[3] = 1.0;
    gLight.specular[0] = 1.0;
    gLight.specular[1] = 1.0;
    gLight.specular[2] = 1.0;
    gLight.specular[3] = 1.0;
    gLight.position[0] = 0;
    gLight.position[1] = 1;
    gLight.position[2] = 0;
    gLight.position[3] = 0.0;

    gLight.Kc = 0.0;
    gLight.Kl = 0.02;
    gLight.Kq = 0.0;
    gLight.on = true;

    setLighting(gLight);
}

void initMaterials()
{
    gMaterial.ambient[0] = 0.15;
    gMaterial.ambient[1] = 0.15;
    gMaterial.ambient[2] = 0.15;
    gMaterial.ambient[3] = 1.0;
    gMaterial.diffuse[0] = 0.20;
    gMaterial.diffuse[1] = 0.20;
    gMaterial.diffuse[2] = 0.20;
    gMaterial.diffuse[3] = 1.0;
    gMaterial.specular[0] = 0.1;
    gMaterial.specular[1] = 0.1;
    gMaterial.specular[2] = 0.1;
    gMaterial.specular[3] = 1.0;
    gMaterial.shininess = 27.0;
}

void localTimer(int value)
{
    static int tickCount = 0;
    gGameLogic->tick(tickCount++);
    glutTimerFunc(1000/60.0, localTimer, value);
}

void* runOpenGL(void *logic)
{
    gGameLogic = reinterpret_cast<GameLogic*>(logic);
    assert(gGameLogic);
    int argc = 0;
    glutInit(&argc, 0);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(W, H);
    glutCreateWindow("Foobar");
    glutFullScreen();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutIdleFunc(idle);
    if (gGameLogic->isLocal())
    {
        glutTimerFunc(1000/60.0, localTimer, 42);
    }

    initLighting();
    initMaterials();

    glutMainLoop();

    return nullptr;
}

void GameLogic::draw() const
{
    glPushMatrix();
    glScaled(1/50.0, 1/50.0, 1/50.0);

    if (mTrack)
    {
        mTrack->draw();
    }

    // sort by standing on the track
    vector<Car*> cars(mCars);
    sort(cars.begin(), cars.end(),
         [](const Car* a, const Car *b){return *b < *a;});

    for (auto car : cars)
    {
        car->draw();
    }

    glPopMatrix();

    // HUD last, on top of everything
    gCarBoxY = 0;
    for (auto car : cars)
    {
        car->drawHUD();
    }
    if (mTrack)
    {
        mTrack->drawHUD();
    }
}

void Track::draw() const
{
    // starting line
    glPushMatrix();
    glBegin(GL_QUADS);
    glColor3d(0.7, 0.7, 0);
    glVertex3d(-5, +30, 0.25);
    glVertex3d(-5, -30, 0.25);
    glVertex3d(+5, -30, 0.25);
    glVertex3d(+5, +30, 0.25);
    glEnd();
    glPopMatrix();

    for (auto& piece : mPieces)
    {
        piece->draw();
    }
}

static string niceCoeff(double x)
{
    if (util::isZero(x))
    {
        return "X";
    }
    stringstream ss;
    ss << (1.0 / x);
    return ss.str();
}

void Track::drawHUD() const
{
    TextBox box(Color::fromName("black"));
    box << mName << "\n";
    const auto& params = getPhysicsParams();
    box << setprecision(4) << fixed;
    box << "Frictn : " << niceCoeff(params.friction.value()) << "\n";
    box << "Power  : " << niceCoeff(params.power.value()) << "\n";
    box << "Gamma  : " << niceCoeff(params.slipDamping.value()) << "\n";
    box << "Omega  : " << niceCoeff(params.slipRestoring.value()) << "\n";
    box << "Tick   : " << gGameLogic->getCurrentTick() << "\n";
    int width, height;
    box.getBoxDimensions(width, height);
    box.setPosition(W-width-10, 10);
    box.draw();
}

void TrackPiece::draw() const
{
    glPushMatrix();
    glTranslated(mStartPosition.x, mStartPosition.y, 0);
    glRotated(mStartRotation, 0, 0, 1);

    // lines separating pieces
    glBegin(GL_LINES);
    glColor3d(23/255.0, 23/255.0, 23/255.0);
    glVertex3d(0, -0.5*mWidth, 0);
    glVertex3d(0, +0.5*mWidth, 0);
    glEnd();
    glPopMatrix();

    // lanes
    const int NUM_SEGMENTS = 10;
    const vector<Lane*>& lanes = mTrack->getLanes();
    for (auto lane : lanes)
    {
        double length = getLaneLength(lane->index);
        glBegin(GL_LINE_STRIP);
        Color::fromName(lane->getColor()).apply();
        for (int i = 0; i <= NUM_SEGMENTS; i++)
        {
            double dist = length * i / NUM_SEGMENTS;
            Vector2D pos = getPositionAlongLane(dist, lane->index);
            glVertex3d(pos.x, pos.y, 0.0);
        }
        glEnd();
    }

    // lane switches
    if (canSwitch())
    {
        for (size_t i = 1; i < lanes.size(); i++)
        {
            const Lane *startLane = lanes[i-1];
            const Lane *endLane = lanes[i];
            const double avgLength = getLaneLength(startLane->index,
                                                   endLane->index);
            glBegin(GL_LINE_STRIP);
            for (int j = 0; j <= NUM_SEGMENTS; j++)
            {
                const double dist = avgLength * j / NUM_SEGMENTS;

                const auto startColor = Color::fromName(startLane->getColor());
                const auto endColor = Color::fromName(endLane->getColor());
                const auto alpha = double(j) / NUM_SEGMENTS;
                const auto lerpColor = startColor*(1-alpha) + endColor*alpha;
                lerpColor.apply();

                Vector2D pos = getPositionAlongLane
                (
                    dist,
                    startLane->index,
                    endLane->index
                );
                glVertex3d(pos.x, pos.y, 0.0);
            }
            glEnd();

            glBegin(GL_LINE_STRIP);
            for (int j = 0; j <= NUM_SEGMENTS; j++)
            {
                const double dist = avgLength * j / NUM_SEGMENTS;

                const auto startColor = Color::fromName(startLane->getColor());
                const auto endColor = Color::fromName(endLane->getColor());
                const auto alpha = double(j) / NUM_SEGMENTS;
                const auto lerpColor = startColor*alpha + endColor*(1-alpha);
                lerpColor.apply();

                Vector2D pos = getPositionAlongLane
                (
                    dist,
                    endLane->index,
                    startLane->index
                );
                glVertex3d(pos.x, pos.y, 0.0);
            }
            glEnd();
        }
    }
}

void TrackPieceStraight::draw() const
{
    TrackPiece::draw();

    glPushMatrix();
    glTranslated(mStartPosition.x, mStartPosition.y, -0.5);
    glRotated(mStartRotation, 0, 0, 1);

    glBegin(GL_QUADS);
    glColor3d(17/255.0, 17/255.0, 17/255.0);
    glVertex3d(0.0, -0.5*mWidth, 0);
    glVertex3d(mLength, -0.5*mWidth, 0);
    glVertex3d(mLength, 0.5*mWidth, 0);
    glVertex3d(0.0, 0.5*mWidth, 0);
    glEnd();
    glPopMatrix();
}

void TrackPieceBend::draw() const
{
    TrackPiece::draw();

    glPushMatrix();
    glTranslated(mStartPosition.x, mStartPosition.y, -0.5);
    glRotated(mStartRotation, 0, 0, 1);

    Vector2D pivot(0, isRightTurn() ? 1 : -1);

    const int NUM_SEGMENTS = 20;
    glBegin(GL_QUAD_STRIP);
    glColor3d(17/255.0, 17/255.0, 17/255.0);
    for (int i = 0; i <= NUM_SEGMENTS; i++)
    {
        Vector2D dir = pivot.rotatedBy(-mArcAngle*i/NUM_SEGMENTS);
        Vector2D near = (mRadius-mWidth/2) * dir - mRadius * pivot;
        Vector2D far = (mRadius+mWidth/2) * dir - mRadius * pivot;
        if (isRightTurn())
        {
            swap(near, far); // to preserve proper vertex winding
        }
        glVertex3d(near.x, near.y, 0);
        glVertex3d(far.x, far.y, 0);
    }
    glEnd();
    glPopMatrix();
}

void Car::draw() const
{
    glPushMatrix();
    glTranslated(getPosition().x, getPosition().y, 0);
    glRotated(getRotation(), 0, 0, 1);
    glTranslated(-mDim.flagPos, 0, mDim.height/2);
    glScaled(mDim.length, mDim.width, mDim.height);
    Color::fromName(mID.color).apply();
    glutSolidCube(1.0);
    glPopMatrix();
}

void Car::drawHUD() const
{
    gCarBoxY += 10;
    TextBox box(Color::fromName(mID.color));
    box.setPosition(10, gCarBoxY);
    box << setprecision(4) << fixed;
    box << mID.name << "\n";
    box << "Speed: " << mStats.speed << "\n";
    box << "Accel: " << mStats.accel << "\n";
    box << "Throt: " << mStats.throttle << "\n";
    box << "Slip : " << mLoc.slipAngle << "\n";
    box << "Lap  : " << mLoc.lap
        << "/" << (mTrack->getTotalLaps()-1) << "\n";
    box << "Piece: " << mLoc.pieceIndex
        << "/" << (mTrack->getPieceCount()-1) << "\n";
    box.setMinLineLength(16);
    box.draw();

    int width, height;
    box.getBoxDimensions(width, height);
    gCarBoxY += height;
}

#endif

