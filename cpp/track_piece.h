#ifndef TRACK_PIECE_H
#define TRACK_PIECE_H

#include "vector2d.h"

class TrackPiece
{
public:
    TrackPiece(bool sw, const Track *track, int index);
    virtual ~TrackPiece() {}
    bool canSwitch() const { return mSwitch; }
    void setStartPosition(const Vector2D& pos) { mStartPosition = pos; }
    void setStartRotation(double rot) { mStartRotation = rot; }
    int getIndex() const { return mPieceIndex; }
    TrackPiece* getNextPiece() const;
    double getLaneDistanceToNextSwitch(int laneIndex) const;
    Vector2D getPositionAlongLane(double dist, int startLaneIndex,
                                  int endLaneIndex) const;
    double getRotationAlongLane(double dist, int startLaneIndex,
                                int endLaneIndex) const;
    double getLaneLength(int startLaneIndex, int endLaneIndex) const;

    virtual Vector2D getEndPosition() const = 0;
    virtual double getEndRotation() const = 0;
    virtual double getLaneLength(int laneIndex) const = 0;
    virtual Vector2D getPositionAlongLane(double dist, int laneIndex) const = 0;
    virtual double getRotationAlongLane(double dist, int laneIndex) const = 0;
    virtual bool isCurve() const = 0;
    virtual double getRadius() const = 0;

#if USE_OPENGL
    virtual void draw() const;
#endif

protected:
    const Track *mTrack;
    int mPieceIndex;
    bool mSwitch; 
    Vector2D mStartPosition;
    double mStartRotation;
    double mWidth;
};

class TrackPieceStraight : public TrackPiece
{
public:
    TrackPieceStraight(double length, bool sw, const Track *track, int index);

    virtual Vector2D getEndPosition() const;
    virtual double getEndRotation() const;
    virtual double getLaneLength(int laneIndex) const;
    virtual Vector2D getPositionAlongLane(double dist, int laneIndex) const;
    virtual double getRotationAlongLane(double dist, int laneIndex) const;
    virtual bool isCurve() const;
    virtual double getRadius() const;

#if USE_OPENGL
    void draw() const;
#endif

private:
    double mLength;
};

class TrackPieceBend : public TrackPiece
{
public:
    TrackPieceBend(double radius, double angle, bool sw, const Track *track,
                   int index);

    virtual Vector2D getEndPosition() const;
    virtual double getEndRotation() const;
    virtual double getLaneLength(int laneIndex) const;
    virtual Vector2D getPositionAlongLane(double dist, int laneIndex) const;
    virtual double getRotationAlongLane(double dist, int laneIndex) const;
    virtual bool isCurve() const;
    virtual double getRadius() const;

    bool isRightTurn() const;
    double getLaneRadius(int laneIndex) const;

#if USE_OPENGL
    void draw() const;
#endif

private:
    double mRadius;
    double mArcAngle;
};

#endif

