#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>

class Car;
class Track;
class TrackPiece;

class GameLogic
{
public:
    GameLogic();
    virtual ~GameLogic();

    virtual bool isLocal() const = 0;
    virtual void tick(int currentTick);
    virtual void cleanUp();

    Car* getCar(const std::string& carName) const;
    Track* getTrack() const { return mTrack; }
    int getCurrentTick() const { return mCurrentTick; }

#if USE_OPENGL
    void draw() const;
#endif

protected:
    Track *mTrack;
    std::vector<Car*> mCars;
    int mCurrentTick;
};

#endif

