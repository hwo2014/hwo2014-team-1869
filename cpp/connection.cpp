#include "connection.h"

HWO_Connection::HWO_Connection(const std::string& host, const std::string& port)
    : mSocket(mService)
{
    tcp::resolver resolver(mService);
    tcp::resolver::query query(host, port);
    boost::asio::connect(mSocket, resolver.resolve(query));
    mResponseBuf.prepare(8192);
}

HWO_Connection::~HWO_Connection()
{
    mSocket.close();
}

jsoncons::json HWO_Connection::receiveResponse(boost::system::error_code& error)
{
    auto len = boost::asio::read_until(mSocket, mResponseBuf, "\n", error);
    if (error)
    {
        return jsoncons::json();
    }
    auto buf = mResponseBuf.data();
    std::string reply(boost::asio::buffers_begin(buf),
                      boost::asio::buffers_begin(buf) + len);
    mResponseBuf.consume(len);
    return jsoncons::json::parse_string(reply);
}

void HWO_Connection::sendRequests(const std::vector<jsoncons::json>& msgs)
{
    jsoncons::output_format format;
    format.escape_all_non_ascii(true);
    boost::asio::streambuf request_buf;
    std::ostream s(&request_buf);
    for (const auto& m : msgs) {
        m.to_stream(s, format);
        s << std::endl;
    }
    mSocket.send(request_buf.data());
}

