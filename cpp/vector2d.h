#ifndef VECTOR2D_H
#define VECTOR2D_H
#include <iostream>
#include <cmath>

#include "util.h"

using namespace std;

class Vector2D
{
public:
    double x, y;

    Vector2D() : x(0), y(0) {}
    Vector2D(const double &_x, const double &_y) : x(_x), y(_y) {}

    Vector2D& operator=(const Vector2D& q) {x=q.x; y=q.y; return *this;}
    Vector2D operator+(const Vector2D& q) const {return Vector2D(x+q.x,y+q.y);}
    void operator+=(const Vector2D& q) {x += q.x; y += q.y;}
    Vector2D operator-(const Vector2D& q) const {return Vector2D(x-q.x,y-q.y);}
    void operator-=(const Vector2D& q) {x -= q.x; y -= q.y;}
    Vector2D operator*(const double &c) const {return Vector2D(c*x,c*y);}
    Vector2D operator*(const int &c) const {return Vector2D(c*x,c*y);}
    double operator*(const Vector2D& q) const {return x*q.x+ y*q.y;}

    double norm2() const {return x*x + y*y;}
    double norm() const {return sqrt(norm2());}

    Vector2D rotatedBy(double angle) const
    {
        const double c = cos(util::deg2rad(angle));
        const double s = sin(util::deg2rad(angle));
        return Vector2D(c*x - s*y, s*x + c*y);
    }

    friend std::ostream & operator<<(std::ostream &flux, const Vector2D &x)
    {
        flux << x.x << "," << x.y;
        return flux;
    }
    friend std::istream & operator>>(std::istream &flux, Vector2D &x)
    {
        flux >> x.x >> x.y;
        return flux;
    }
};

Vector2D operator*(double c, const Vector2D &A);

#endif

