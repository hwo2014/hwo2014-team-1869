#ifndef HWO_CONNECTION_H
#define HWO_CONNECTION_H

#include <string>
#include <iostream>
#include <boost/asio.hpp>
#include <jsoncons/json.hpp>

using boost::asio::ip::tcp;

class HWO_Connection
{
public:
    HWO_Connection(const std::string& host, const std::string& port);
    ~HWO_Connection();
    jsoncons::json receiveResponse(boost::system::error_code& error);
    void sendRequests(const std::vector<jsoncons::json>& msgs);

private:
    boost::asio::io_service mService;
    tcp::socket mSocket;
    boost::asio::streambuf mResponseBuf;
};

#endif
