#ifndef TRACK_H
#define TRACK_H

#include <iostream>
#include <string>
#include <vector>
#include <jsoncons/json.hpp>

#include "truncated_estimator.h"

struct PhysicsParameters
{
    PhysicsParameters();

    // linear motion dynamics
    TruncatedEstimator friction;
    TruncatedEstimator power;

    // slip dynamics
    TruncatedEstimator slipDamping;
    TruncatedEstimator slipRestoring;

    struct Turbo
    {
        Turbo() : available(false), durationTicks(), factor() {}
        bool available;
        int durationTicks;
        double factor;
    } turbo;
};

class TrackPiece;
class Lane;

class Track
{
public:
    Track(const jsoncons::json& data);

    const std::string& getId() const { return mId; }
    const std::vector<Lane*>& getLanes() const { return mLanes; }
    const Lane* getLane(int laneIndex) const;
    TrackPiece* getPiece(int pieceIndex) const { return mPieces[pieceIndex]; }
    int getPieceCount() const { return mPieces.size(); }

    PhysicsParameters& getPhysicsParams() { return mPhysicsParams; }
    const PhysicsParameters& getPhysicsParams() const { return mPhysicsParams; }

    void setTotalLaps(int laps) { mTotalLaps = laps; }
    int getTotalLaps() const { return mTotalLaps; }

#if USE_OPENGL
    void draw() const;
    void drawHUD() const;
#endif

private:
    std::string mId;
    std::string mName;
    std::vector<TrackPiece*> mPieces;
    std::vector<Lane*> mLanes;

    int mTotalLaps;
    PhysicsParameters mPhysicsParams;
};

#endif

